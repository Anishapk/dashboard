from django.shortcuts import render


# Create your views here.

def index(request):
    return render(request, 'admin_app/index.html')


def all_students(request):
    return render(request, 'admin_app/all_students.html')


# def add_department(request):
#     return render(request, 'admin_app/ add_department.html')


# def add_library_assets(request):
#     return render(request, 'admin_app/add_library_assets.html')


# def add_professor(request):
#     return render(request, 'admin_app/add_professor.html')


# def add_students(request):
#     return render(request, 'admin_app/add_students.html')


# def all_courses(request):
#     return render(request, 'admin_app/all_courses.html')


# def all_professors(request):
#     return render(request, 'admin_app/all_professors.html')


# def all_students(request):
#     return render(request, 'admin_app/all_students.html')


# def analytics(request):
#     return render(request, 'admin_app/analytics.html')


# def course_info(request):
#     return render(request, 'admin_app/course_info.html')


# def course_payment(request):
#     return render(request, 'admin_app/course_payment.html')


# def departments(request):
#     return render(request, 'admin_app/departments.html')


# def edit_course(request):
#     return render(request, 'admin_app/edit_course.html')


# def edit_department(request):
#     return render(request, 'admin_app/edit_department.html')


# def edit_library_assets(request):
#     return render(request, 'admin_app/edit_library_assets.html')


# def edit_professor(request):
#     return render(request, 'admin_app/edit_professor.html')


# def edit_student(request):
#     return render(request, 'admin_app/edit_student.html')


# def edit_events(request):
#     return render(request, 'admin_app/edit_events.html')


# def library_assets(request):
#     return render(request, 'admin_app/library_assets.html')


# def login(request):
#     return render(request, 'admin_app/login.html')


# def register(request):
#     return render(request, 'admin_app/register.html')


# def student_profile(request):
#     return render(request, 'admin_app/student_profile.html')


