from django.urls import path

from . import views

app_name = 'admin_app'
urlpatterns = [
    path('', views.index, name=''),
    path('all_students/', views.all_students, name='all-students'),

    # path('add_course', views.add_course, name='add_course'),
    # path('add_department', views.add_department, name='add_department'),
    # path('add_library_assets', views.add_library-assets, name='add_library_assets'),
    # path('add_professor', views.add_professor, name='add_professor'),
    # path('add_students', views.add_students, name='add_students'),
    # path('all_courses', views.all_courses, name='all_courses'),
    # path('all_professors', views.all_professors, name='all_professors'),
    # path('analytics', views.analytics, name='analytics'),
    # path('course_info', views.course_info, name='course_info'),
    # path('course_payment', views.course_payment, name='course_payment'),
    # path('departments', views.departments, name='departments'),
    # path('edit_course', views.edit_course, name='edit_course'),
    # path('edit_department', views.edit_department, name='edit_department'),
    # path('edit_library_assets', views.edit_library_assets, name='edit_library_assets'),
    # path('edit_professor', views.edit_professor, name='edit_professor'),
    # path('edit_student', views.edit_student, name='edit_student'),
    # path('events', views.events, name='events'),
    # path('library_assets', views.library_assets, name='library_assets'),
    # path('login', views.login, name='login'),
    # path('register', views.register, name='register'),
    # path('student_profile', views.student_profile, name='student_profile'),

]