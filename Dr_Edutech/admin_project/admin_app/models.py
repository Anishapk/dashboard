from django.db import models


# Create your models here.

#class admin_info(models.Model):
    #image = models.ImageField(upload_to='meta-images/', blank=True)
    #name = models.CharField(max_length=50, default="", blank=True)

    #def __str__(self):
        #return self.name

class user_info(models.Model):
    
    name = models.CharField(max_length=50, default="", blank=False)
    mail_id = models.EmailField(max_length=50, default="", blank=False)
    mobile_number = models.IntegerField(default="", blank=True)
    password = models.CharField(max_length=50, default="", blank=False)
    
    def __str__(self):
        return self.name


class user_profile(models.Model):
    
    #image = models.ImageField(upload_to='/', blank=True)
    skillset = models.TextField(max_length=2500, default=None)
    

    #def __str__(self):
        #return self.name


class user_certification(models.Model):
    
    #image = models.ImageField(upload_to='/', blank=True)
    course_name = models.CharField(max_length=50, default="", blank=True)
    score = models.IntegerField(default="", blank=False)
    date = models.DateField(blank=False)

    def __str__(self):
        return self.course_name


class course_info(models.Model):
    
    course_name = models.CharField(max_length=50, default="", blank=True)
    # image = models.ImageField(upload_to='/', blank=True)
    start_date = models.DateField(blank=False)
    duration = models.TimeField(blank=False)
    price = models.IntegerField(default=0, blank=False)
    category = models.CharField(max_length=50, default="", blank=False)
    instructor = models.CharField(max_length=50, default="", blank=False)
    year = models.DateField(blank=False)
    description = models.TextField(max_length=2500, default=None)
    
    
    def __str__(self):
        return self.course_name


class course_description(models.Model):
    
    course_outline = models.TextField(max_length=2500, default=None)
    topics = models.TextField(max_length=2500, default=None)
    reqiurements = models.TextField(max_length=2500, default=None)
    webinar_url =  models.URLField(max_length=500)

    def __str__(self):
        return self.topics


class webinar(models.Model):
    
    duration = models.TimeField(blank=False)
    date = models.DateField(blank=False)
    name = models.CharField(max_length=50, default="", blank=False)
    instructor = models.CharField(max_length=50, default="", blank=True)

    def __str__(self):
        return self.name


class Instructor_info(models.Model):
    
    name = models.CharField(max_length=50, default="", blank=False)
    specialization = models.CharField(max_length=200, default="", blank=False)
    description = models.TextField(max_length=2500, default=None)
    rating = models.IntegerField(default=0, blank=False)
    # image = models.ImageField(upload_to='/', blank=True)
    phone_number = models.IntegerField(default="", blank=True)
    password = models.CharField(max_length=50, default="", blank=False)
    category = models.CharField(max_length=50, default="", blank=False)

    def __str__(self):
        return self.name


class video_info(models.Model):
    
    name = models.CharField(max_length=50, default="", blank=False)
    video_url =  models.URLField(max_length=500)
    duration = models.TimeField(blank=False)
    instructor = models.CharField(max_length=50, default="", blank=True)
    uploaded_date = models.DateField(blank=False)
    rating = models.IntegerField(default=0, blank=False)
    category = models.CharField(max_length=50, default="", blank=False)
    description = models.TextField(max_length=2500, default=None)

    def __str__(self):
        return self.name

class video_description(models.Model):

    video_outline = models.TextField(max_length=2500, default=None)
    topics = models.TextField(max_length=2500, default=None)
    reqiurements = models.TextField(max_length=2500, default=None)
    webinar_url =  models.URLField(max_length=500)

    def __str__(self):
        return self.topics

class category(models.Model):

    category_name = models.CharField(max_length=50, default="", blank=False)
    description = models.TextField(max_length=2500, default=None)

    def __str__(self):
        return self.category_name


class subcategory(models.Model):
    
    category_name = models.CharField(max_length=50, default="", blank=False)
    subcategory_name = models.CharField(max_length=50, default="", blank=False)
    description = models.TextField(max_length=2500, default=None)

    def __str__(self):
        return self.category_name
















